const express = require("express");
const path = require("path");
const cors = require("cors");
const compression = require("compression");

const app = express();
const PORT = process.env.PORT || 8080;
const YEAR = 31536000;

const allowedOrigins = [
  /^http:\/\/localhost(:\d+)?$/,
  /^https?:\/\/.*\.web\.cern\.ch$/,
];

const corsOptions = {
  origin: '*',
  methods: ["GET"],
  allowedHeaders: ["Content-Type"],
  credentials: true,
};

app.use(cors(corsOptions));
app.use(compression());
app.use((_, res, next) => {
  res.setHeader("Cache-Control", `max-age=${YEAR}`);
  next();
});
app.use(express.static(path.join(__dirname, "models")));

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
