const fs = require("fs");
const path = require("path");

const directoryPath = path.join(__dirname);

fs.readdir(directoryPath, function (err, files) {
  if (err) {
    console.error("Error reading directory:", err);
    return;
  }

  files.forEach(function (file) {
    const oldPath = path.join(directoryPath, file);
    const newName = file.toLowerCase();
    const newPath = path.join(directoryPath, newName);

    fs.rename(oldPath, newPath, function (err) {
      if (err) {
        console.error("Error renaming file:", err);
        return;
      }

      console.log(`File ${oldPath} renamed to ${newPath}`);
    });
  });
});
