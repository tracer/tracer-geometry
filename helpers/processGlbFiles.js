const fs = require("fs");
const path = require("path");
const { execSync } = require("child_process");

const directoryPath = path.join(__dirname);

fs.readdir(directoryPath, (err, files) => {
  if (err) {
    console.error("Error reading directory:", err);
    return;
  }

  files.forEach((file) => {
    if (path.extname(file) === ".glb") {
      const filePath = path.join(directoryPath, file);
      const command = `npx gltfjsx ${filePath} --transform --t`;
      const deleteCommand = `rm ${filePath}`;

      try {
        execSync(command, { stdio: "inherit" });
        execSync(deleteCommand, { stdio: "inherit" });
      } catch (error) {}
    }
  });
});
