const fs = require("fs");
const path = require("path");

const srcFolder = path.join(__dirname);
const destFolder = `${srcFolder}/../tsx`;

fs.readdir(srcFolder, (err, files) => {
  if (err) {
    throw err;
  }

  files.forEach((file) => {
    if (path.extname(file) === ".tsx") {
      const srcPath = path.join(srcFolder, file);
      const destPath = path.join(destFolder, file);

      fs.rename(srcPath, destPath, (err) => {
        if (err) {
          throw err;
        }
        console.log(`${file} moved to ${destFolder}`);
      });
    }
  });
});
